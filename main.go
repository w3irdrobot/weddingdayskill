package main

import (
	"fmt"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
)

type outputSpeechType string

const (
	plainText outputSpeechType = "PlainText"
)

type alexaResponse struct {
	Version  string   `json:"version"`
	Response response `json:"response"`
}

type response struct {
	OutputSpeech     outputSpeech `json:"outputSpeech"`
	ShouldEndSession bool         `json:"shouldEndSession"`
}

type outputSpeech struct {
	Type outputSpeechType `json:"type"`
	Text string           `json:"text"`
}

func newAlexaResponse(speechType outputSpeechType, text string) alexaResponse {
	return alexaResponse{
		Version: "1.0",
		Response: response{
			OutputSpeech: outputSpeech{
				Type: speechType,
				Text: text,
			},
			ShouldEndSession: true,
		},
	}
}

func handler() (alexaResponse, error) {
	easternTz, err := time.LoadLocation("America/New_York")
	if err != nil {
		panic(err)
	}

	weddingDay := time.Date(2019, time.January, 19, 18, 0, 0, 0, easternTz)
	now := time.Now().In(easternTz)
	timeLeft := weddingDay.Sub(now)
	daysLeft := int(timeLeft.Hours() / 24)

	if daysLeft > 0 {
		return newAlexaResponse(plainText, fmt.Sprintf("There are %d days until your wedding day.", daysLeft)), nil
	} else if daysLeft == 0 {
		return newAlexaResponse(plainText, fmt.Sprintf("Today is your wedding day!")), nil
	} else {
		return newAlexaResponse(plainText, fmt.Sprintf("You're already married, silly.")), nil
	}
}

func main() {
	lambda.Start(handler)
}
